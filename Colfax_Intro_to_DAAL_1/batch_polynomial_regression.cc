/* /////////////////////////////////////////////////
This is an example application for the publication:
Asai, Ryo, "Introduction to Intel DAAL: Polynomial Regression
with Batch Mode Computation", (2015) Colfax Research

The publication is available for download at:
http://colfaxresearch.com/intro-to-daal-1/
*///////////////////////////////////////////////////

#include <stdlib.h>
#include <daal.h>
#include <omp.h>
#include <math.h>
using namespace std;
using namespace daal;
using namespace daal::data_management;
using namespace daal::algorithms::linear_regression;
  
// Polynomial expansion of the feature vector
void expand_feature_vector(double* feature_vals, double* expanded_features, int nFeatures, int nVectors, int expansion) {  
  for(int i = 0; i < nVectors; i++) {
    for(int j = 0; j < nFeatures; j++) {
      const double val = feature_vals[i*nFeatures + j];
      double powers = val;
      for(int k = 0; k < expansion; k++) {
	expanded_features[i*nFeatures*expansion + j*expansion + k] = powers;
	powers *= val;
      }
    }
  }
}


int main(int argc, char* argv[])
{
  if(argc < 7) {
    printf("usage: %s [trnFeaturesFile] [trnResponseFile] [nTrnVectors] [testFeaturesFile] [nTstVectors] [expansion]\n", argv[0]);
    return 1;
  }

  // Parsing the arguments
  const string trainingFeaturesFile(argv[1]);  
  const string trainingResponseFile(argv[2]);
  const int nTrnVectors = atoi(argv[3]);
  const string testFeaturesFile(argv[4]);
  const int nTstVectors = atoi(argv[5]);
  const int expansion = atoi(argv[6]);
  
  // Create the data source for teh input CSV files
  FileDataSource<CSVFeatureManager> trainingFeaturesSource(trainingFeaturesFile,
					       DataSource::doAllocateNumericTable,
					       DataSource::doDictionaryFromContext);
  FileDataSource<CSVFeatureManager> trainingResponseSource(trainingResponseFile,
					       DataSource::doAllocateNumericTable,
					       DataSource::doDictionaryFromContext);
  FileDataSource<CSVFeatureManager> testFeaturesSource(testFeaturesFile,
					       DataSource::doAllocateNumericTable,
					       DataSource::doDictionaryFromContext);

  // Load data from the CSV file
  trainingFeaturesSource.loadDataBlock(nTrnVectors);
  trainingResponseSource.loadDataBlock(nTrnVectors);
  testFeaturesSource.loadDataBlock(nTstVectors);
  
  // Finding the number of Features and Responses
  const int nFeatures  = trainingFeaturesSource.getNumberOfColumns();
  const int nResponses = trainingResponseSource.getNumberOfColumns();

  // Numeric tables that will contain the data sets
  services::SharedPtr<NumericTable> trainingFeaturesTable;
  services::SharedPtr<NumericTable> trainingResponseTable;
  services::SharedPtr<NumericTable> testFeaturesTable;

  if(expansion > 0) {
    printf("Expanding the features to %d-th order\n", expansion);
    // Response does not need to be expanded
    trainingResponseTable = trainingResponseSource.getNumericTable();

    // Expanding training features for polynomial regression
    BlockDescriptor<double> trnFeatures_block;
    trainingFeaturesSource.getNumericTable()->getBlockOfRows(0, nTrnVectors, readOnly, trnFeatures_block);
    double * expanded_trnFeatures = (double*) malloc(sizeof(double)*nFeatures*expansion*nTrnVectors);
    expand_feature_vector(trnFeatures_block.getBlockPtr(), expanded_trnFeatures, nFeatures, nTrnVectors, expansion);
    trainingFeaturesTable = services::SharedPtr<NumericTable>(new HomogenNumericTable<double>(expanded_trnFeatures, nFeatures*expansion, nTrnVectors));

    // Expanding test features for polynomial regression
    BlockDescriptor<double> tstFeatures_block;
    testFeaturesSource.getNumericTable()->getBlockOfRows(0, nTstVectors, readOnly, tstFeatures_block);
    double * expanded_tstFeatures = (double*) malloc(sizeof(double)*nFeatures*expansion*nTstVectors);
    expand_feature_vector(tstFeatures_block.getBlockPtr(), expanded_tstFeatures, nFeatures, nTstVectors, expansion);
    testFeaturesTable = services::SharedPtr<NumericTable>(new HomogenNumericTable<double>(expanded_tstFeatures, nFeatures*expansion, nTstVectors));
  } else {
    // Invalid expansion values. Not expanding the features to a polynomial 
    trainingFeaturesTable = trainingFeaturesSource.getNumericTable();
    trainingResponseTable = trainingResponseSource.getNumericTable();
    testFeaturesTable = testFeaturesSource.getNumericTable();
  }

  // Training
  training::Batch<> algorithm;
  algorithm.input.set(training::data, trainingFeaturesTable);
  algorithm.input.set(training::dependentVariables, trainingResponseTable);
  printf("Training with %d sets....", nTrnVectors); fflush(0);
  const double t0 = omp_get_wtime();
  algorithm.compute();
  const double t1 = omp_get_wtime();
  printf(" Done (%fs)\n", t1-t0); fflush(0);  
  
  // Extracting training result
  services::SharedPtr<training::Result> trainingResult;  
  trainingResult= algorithm.getResult();

  // Predictions     
  prediction::Batch<> predictor;
  predictor.input.set(prediction::data, testFeaturesTable);
  predictor.input.set(prediction::model, trainingResult->get(training::model));
  predictor.compute();

  // Extracting prediction rsesult
  services::SharedPtr<prediction::Result> predictionResult;
  predictionResult = predictor.getResult();
  
  // Acquiring the pointer to the result array
  BlockDescriptor<double> result_block;
  predictionResult->get(prediction::prediction)->getBlockOfRows(0, 1, readOnly, result_block);
  double *result = result_block.getBlockPtr();
  
  // Printing the predictions. 
  for(int i = 0; i < nTstVectors; i++) {
    printf("%f", result[i*nResponses]);
    for(int j = 1; j < nResponses; i++) {
      printf(",%f", result[i*nResponses+j]);
    }
    printf("\n");
  }
    
  return 0;
}

# For converting the yacht data to CSV files
# The original data file can be found at
# 
if [[ -z $1 ]]; then
    echo This script is for converting the yacht hydrodynamics data set
    echo to the CSV format used by the polynomial regression application 
    echo
    echo Usage: $0 [yacht data file]
    exit
fi

ORIGFILE=$1

TRNFEATURESFILE=trnFeatures.csv
TRNRESPONSEFILE=trnResponse.csv
TSTFEATURESFILE=tstFeatures.csv
REFRESPONSEFILE=refResponse.csv

if [[ -e $TRNFEATURESFILE || -e $TRNRESPONSEFILE || \
      -e $TSTFEATURESFILE || -e $REFRESPONSEFILE ]]; then
    echo Following files will be over written: 
    echo $TRNFEATURESFILE $TRNRESPONSEFILE $TSTFEATURESFILE $REFRESPONSEFILE
    echo "Continue? (hit any key to continue, Ctrl-C to quit)"
    read t
    rm $TRNFEATURESFILE $TRNRESPONSEFILE $TSTFEATURESFILE $REFRESPONSEFILE
fi

# Parsing the file to separate the data into features and responses
cat $ORIGFILE | awk '{printf "%.1f,%.3f,%.2f,%.2f,%.2f,%.3f\n", $1,$2,$3,$4,$5,$6}' >> $TRNFEATURESFILE
cat $ORIGFILE | awk '{printf "%.2f\n", $7}' >> $TRNRESPONSEFILE
# deleting the empty line at the eond of file
LASTLN=`cat $TRNFEATURESFILE | wc -l`
sed -i -e $LASTLN'd' $TRNFEATURESFILE
sed -i -e $LASTLN'd' $TRNRESPONSEFILE

# Setting aside 7 data points for testing
TSTFEATUREVECTORS=7
LCOUNT=`cat $ORIGFILE | wc -l`
for i in `seq 1 $TSTFEATUREVECTORS`; do
    RNDPOINT=$(( $RANDOM % $LCOUNT ))
    cat $TRNFEATURESFILE | tail -n +$RNDPOINT | head -n +1 >> $TSTFEATURESFILE
    sed -i -e $RNDPOINT'd' $TRNFEATURESFILE 
    cat $TRNRESPONSEFILE | tail -n +$RNDPOINT | head -n +1 >> $REFRESPONSEFILE
    sed -i -e $RNDPOINT'd' $TRNRESPONSEFILE 
    LCOUNT=$(( $LCOUNT - 1 )) 
done
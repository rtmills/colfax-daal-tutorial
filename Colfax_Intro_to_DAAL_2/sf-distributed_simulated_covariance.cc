#include <stdlib.h>
#include <daal.h>
#include <omp.h>
#include <math.h>
#include <mpi.h>
#include <mkl.h>
#include <limits.h>
/*
  Include petscsf.h so we can use PetscSF objects. Note that this automatically
  includes petscsys.h.
*/
#include <petscsf.h>
#include <petscviewer.h>

using namespace std;
using namespace daal;
using namespace daal::data_management;
using namespace daal::algorithms;


// Populating the data matrix with random vectors.
const double featureMax = 3.0;
void generate_random_matrix(double* matrix, long nVariables, long nVectors, int seed) {
  const long threads = (long)omp_get_max_threads();
  const long valsPerTh = nVectors*nVariables/threads;
  const long overflow = nVectors*nVariables%threads;
#pragma omp parallel
  {
    const long th = (long)omp_get_thread_num();
    VSLStreamStatePtr rnStream;  
    vslNewStream( &rnStream, VSL_BRNG_MT19937, seed );
    long populated = 0;
    while(populated < valsPerTh){
      const MKL_INT size = (valsPerTh-populated < INT_MAX) ? valsPerTh-populated : INT_MAX;
      vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD, rnStream, size, &matrix[th*valsPerTh+populated], -1.0*featureMax, featureMax);
      populated += size;
    }
#pragma omp single
    {
      vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD, rnStream, overflow, &matrix[threads*valsPerTh], -1.0*featureMax, featureMax);
    }
  }
}


int main(int argc, char* argv[])
{
  PetscErrorCode ierr;
  PetscSF        sf;
  PetscLayout    layout;
  PetscInt       nroots,nleaves;
  PetscBool      use_sf = PETSC_FALSE;

  /* Initialize PETSc and parse command line options specific to this example. */
  ierr = PetscInitialize(&argc,&argv,(char*)0,PETSC_NULL);if (ierr) return ierr;
  ierr = PetscOptionsBegin(PETSC_COMM_WORLD,"","PetscSF Test Options","none");CHKERRQ(ierr);
  ierr = PetscOptionsBool("-use_sf","Use PetscSF to communicate partial results","",use_sf,&use_sf,NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  // Determine name, rank and total number of processes.
  int worldSize, rank, namelen;
  MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // Boss Rank (rank = 0) parsing arguments.
  long nVariables, nVectors, totalVectors;
  if (rank == 0){
    if(argc < 3) {
      printf("Usage: %s {nVariables} {nVectors} \n", argv[0]);
      MPI_Abort(MPI_COMM_WORLD, -1);
    } else {
      nVariables = atol(argv[1]);
      totalVectors = atol(argv[2]);
      nVectors = totalVectors/worldSize;
    }
  }

  // Distributing the workload
  MPI_Bcast(&nVariables, 1, MPI_LONG, 0, MPI_COMM_WORLD);
  MPI_Bcast(&nVectors, 1, MPI_LONG, 0, MPI_COMM_WORLD);
  if(rank == 0){
    nVectors += totalVectors%worldSize; //Boss doing the extra work
    printf("Computing %ldx%ld covariance matrices from %d vectors each on %d processes (%d total vectors).\n", nVariables, nVariables, nVectors, worldSize, totalVectors); fflush(0);
  }  

  // Generating data
  double* dataMatrix = (double*) malloc(sizeof(double)*nVectors*nVariables); 
  generate_random_matrix(dataMatrix, nVariables, nVectors, 0);
  services::SharedPtr<NumericTable> dataTable(new HomogenNumericTable<double>(dataMatrix, nVariables, nVectors));

  // Local computation and getting the PartialResult 
  covariance::Distributed<step1Local> algorithm;
  algorithm.input.set(covariance::data, dataTable);
  algorithm.compute();
  services::SharedPtr<covariance::PartialResult> covariancePartResult;  
  covariancePartResult = algorithm.getPartialResult();
  
  if (rank) {

    /******************************  W O R K E R  ******************************/ 

    // Serialization of partial results
    InputDataArchive partResultArchive;
    covariancePartResult->serialize(partResultArchive);
    size_t buffer_size = partResultArchive.getSizeOfArchive();
    byte send_buffer[buffer_size] __attribute__((aligned(4096)));
    // byte* send_buffer=(byte*)_mm_malloc(buffer_size, 4096); // Use if the buffer is too large
    partResultArchive.copyArchiveToArray(send_buffer, buffer_size);
    
    /* Note: I've left the original MPI_Send() call in here used in the Colfax example to communicate the buffer
     * sizes, but note that there is a nicer way to do this using the PetscLayout object that we create later.
     * This really should be done by using PetscLayoutGetRanges() on the master rank. */
    // Sending the size of my buffer
    MPI_Send(&buffer_size, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);

    if (use_sf) { /********** Use PetscSF to send results **********/
      /* Now create a PetscLayout and PetscSF that will be used to gather the partial results. */
      ierr = PetscLayoutCreateFromSizes(PETSC_COMM_WORLD,buffer_size,PETSC_DECIDE,1,&layout);CHKERRQ(ierr);
      ierr = PetscSFCreate(PETSC_COMM_WORLD,&sf);CHKERRQ(ierr);
      ierr = PetscSFSetFromOptions(sf);CHKERRQ(ierr);
      ierr = PetscSFSetGraphWithPattern(sf,layout,PETSCSF_PATTERN_GATHER);CHKERRQ(ierr);
      ierr = PetscSFSetUp(sf);CHKERRQ(ierr);
      /* View graph, mostly useful for debugging purposes. */
      ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_INFO_DETAIL);CHKERRQ(ierr);
      ierr = PetscSFView(sf,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

      /* Now communicate the partial results. */
      ierr = PetscSFBcastBegin(sf,MPI_BYTE,send_buffer,PETSC_NULL,MPI_REPLACE);CHKERRQ(ierr);
      ierr = PetscSFBcastEnd(sf,MPI_BYTE,send_buffer,PETSC_NULL,MPI_REPLACE);CHKERRQ(ierr);
    } else {
      // Send my partial result
      MPI_Send(send_buffer, buffer_size, MPI_BYTE, 0, 0, MPI_COMM_WORLD);
    }
  } else {

    /******************************  M A S T E R  ******************************/

    printf("Boss worker done.\n"); fflush(0);
    // Adding Boss worker's portion
    covariance::Distributed<step2Master> algorithmMaster;
    // algorithmMaster.parameter.outputMatrixType = covariance::correlationMatrix; // Use this for Correlation
    algorithmMaster.input.add(covariance::partialResults, covariancePartResult); 

    // Gathering all the results together
    MPI_Status stat;

    /* First figure out how much data will be sent by each process. */
    int buffer_size;
    int buffer_sizes[worldSize]; /* Yes, element 0 corresponds to rank 0 and isn't actually needed. */
    int total_buffer_size = 0;
    for(int i = 1; i < worldSize; i++) {
      MPI_Recv(&buffer_size, 1, MPI_INT, i, 0, MPI_COMM_WORLD, &stat);
      buffer_sizes[i] = buffer_size;
      total_buffer_size += buffer_sizes[i];
    }

    if (use_sf) { /********** Use PetscSF to gather results **********/
      /* Participate in creation of the PetscLayout and PetscSF. Rank 0's size in the layout will be 0. */
      ierr = PetscLayoutCreateFromSizes(PETSC_COMM_WORLD,0,PETSC_DECIDE,1,&layout);CHKERRQ(ierr);
      ierr = PetscSFCreate(PETSC_COMM_WORLD,&sf);CHKERRQ(ierr);
      ierr = PetscSFSetFromOptions(sf);CHKERRQ(ierr);
      ierr = PetscSFSetGraphWithPattern(sf,layout,PETSCSF_PATTERN_GATHER);CHKERRQ(ierr);
      ierr = PetscSFSetUp(sf);CHKERRQ(ierr);
      /* View graph, mostly useful for debugging purposes. */
      ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD,PETSC_VIEWER_ASCII_INFO_DETAIL);CHKERRQ(ierr);
      ierr = PetscSFView(sf,PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
      ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);

      /* Now communicate the partial results. 
       * Note that the receive buffer contains the "leaves" and the send buffers on the workers are the "roots";
       * this is the convention adopted by PetscSFSetGraphWithPattern() when using PETSCSF_PATTERN_GATHER. */
      ierr = PetscPrintf(PETSC_COMM_WORLD,"Using PetscSF to communicate partial results.\n");CHKERRQ(ierr);
      byte recv_buffer[total_buffer_size] __attribute__((aligned(4096)));
      ierr = PetscSFBcastBegin(sf,MPI_BYTE,PETSC_NULL,recv_buffer,MPI_REPLACE);CHKERRQ(ierr);
      ierr = PetscSFBcastEnd(sf,MPI_BYTE,PETSC_NULL,recv_buffer,MPI_REPLACE);CHKERRQ(ierr);

      for(int i = 1; i < worldSize; i++) {
        byte *buffer_loc = recv_buffer;
        // Deserializing the received partial result and then adding it.
        OutputDataArchive receivedArchive(buffer_loc, buffer_sizes[i]); 
        services::SharedPtr<covariance::PartialResult> receivedPartResult = services::SharedPtr<covariance::PartialResult>(new covariance::PartialResult());
        receivedPartResult->deserialize(receivedArchive);
        algorithmMaster.input.add(covariance::partialResults, receivedPartResult);
        ierr = PetscPrintf(PETSC_COMM_WORLD,"Using SF: Added result from rank %d.\n", i);CHKERRQ(ierr); fflush(0);
        buffer_loc += buffer_sizes[i];
      }
    } else { /********** Use raw MPI to receive results. **********/
      /* Now receive the partial results and process them. */
      ierr = PetscPrintf(PETSC_COMM_WORLD,"Using raw MPI to communicate partial results.\n");CHKERRQ(ierr);
      for(int i = 1; i < worldSize; i++) {
        // This is unnecessary allocation in our particular situation because buffer_size is always the
        // same. However, if the buffer_size changes from one process to another, this is required.
        byte recv_buffer[buffer_sizes[i]] __attribute__((aligned(4096)));
        //byte* recv_buffer=(byte*)_mm_malloc(buffer_size, 4096); // Use if the buffer is too large
        MPI_Recv(recv_buffer, buffer_sizes[i], MPI_BYTE, i, 0, MPI_COMM_WORLD, &stat);

        // Deserializing the received partial result and then adding it.
        OutputDataArchive receivedArchive(recv_buffer, buffer_sizes[i]); 
        services::SharedPtr<covariance::PartialResult> receivedPartResult = services::SharedPtr<covariance::PartialResult>(new covariance::PartialResult());
        receivedPartResult->deserialize(receivedArchive);
        algorithmMaster.input.add(covariance::partialResults, receivedPartResult);
        printf("Received and added result from rank %d.\n", i); fflush(0);
      }
    }

    // Master computation.
    algorithmMaster.compute();


    algorithmMaster.finalizeCompute();
    services::SharedPtr<covariance::Result> covarianceResult;  
    covarianceResult= algorithmMaster.getResult();
    printf("Master computation complete.\n"); fflush(0);
    
    // Extracting the result.
    BlockDescriptor<double> result_block;
    covarianceResult->get(covariance::covariance)->getBlockOfRows(0, nVariables, readOnly, result_block);
    covarianceResult->get(covariance::correlation)->getBlockOfRows(0, nVariables, readOnly, result_block); // Use this for Correlation
    double *result = result_block.getBlockPtr();
    
    // Printing the part of the result.
    const int outputN = (10 < nVariables) ? 10 : nVariables;  
    printf("Printing the top left %dx%d corner\n", outputN, outputN);
    for(int i = 0; i < outputN; i++) {
      for(int j = 0; j < outputN; j++) {
	printf("%f ", result[i*nVariables+j]);
      } 
      printf("\n");
    } 
      
  } 
  
  MPI_Finalize();
  return 0;
}

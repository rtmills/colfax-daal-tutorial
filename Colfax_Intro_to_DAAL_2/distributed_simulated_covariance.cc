#include <stdlib.h>
#include <daal.h>
#include <omp.h>
#include <math.h>
#include <mpi.h>
#include <mkl.h>
#include <limits.h>

using namespace std;
using namespace daal;
using namespace daal::data_management;
using namespace daal::algorithms;


// Populating the data matrix with random vectors.
const double featureMax = 3.0;
void generate_random_matrix(double* matrix, long nVariables, long nVectors, int seed) {
  const long threads = (long)omp_get_max_threads();
  const long valsPerTh = nVectors*nVariables/threads;
  const long overflow = nVectors*nVariables%threads;
#pragma omp parallel
  {
    const long th = (long)omp_get_thread_num();
    VSLStreamStatePtr rnStream;  
    vslNewStream( &rnStream, VSL_BRNG_MT19937, seed );
    long populated = 0;
    while(populated < valsPerTh){
      const MKL_INT size = (valsPerTh-populated < INT_MAX) ? valsPerTh-populated : INT_MAX;
      vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD, rnStream, size, &matrix[th*valsPerTh+populated], -1.0*featureMax, featureMax);
      populated += size;
    }
#pragma omp single
    {
      vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD, rnStream, overflow, &matrix[threads*valsPerTh], -1.0*featureMax, featureMax);
    }
  }
}


int main(int argc, char* argv[])
{
  // MPI initialization
  int ret = MPI_Init(&argc,&argv);
  if (ret != MPI_SUCCESS) {
    printf("error: could not initialize MPI\n");
    MPI_Abort(MPI_COMM_WORLD, ret);
  }

  // Determine name, rank and total number of processes.
  int worldSize, rank, namelen;
  MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  // Boss Rank (rank = 0) parsing arguments.
  long nVariables, nVectors, totalVectors;
  if (rank == 0){
    if(argc < 3) {
      printf("Usage: %s {nVariables} {nVectors} \n", argv[0]);
      MPI_Abort(MPI_COMM_WORLD, ret);
    } else {
      nVariables = atol(argv[1]);
      totalVectors = atol(argv[2]);
      nVectors = totalVectors/worldSize;
    }
  }

  // Distributing the workload
  MPI_Bcast(&nVariables, 1, MPI_LONG, 0, MPI_COMM_WORLD);
  MPI_Bcast(&nVectors, 1, MPI_LONG, 0, MPI_COMM_WORLD);
  if(rank == 0){
    nVectors += totalVectors%worldSize; //Boss doing the extra work
    printf("Computing %ldx%ld covariance matrices from %d vectors each on %d processes (%d total vectors).\n", nVariables, nVariables, nVectors, worldSize, totalVectors); fflush(0);
  }  

  // Generating data
  double* dataMatrix = (double*) malloc(sizeof(double)*nVectors*nVariables); 
  generate_random_matrix(dataMatrix, nVariables, nVectors, 0);
  services::SharedPtr<NumericTable> dataTable(new HomogenNumericTable<double>(dataMatrix, nVariables, nVectors));

  // Local computation and getting the PartialResult 
  covariance::Distributed<step1Local> algorithm;
  algorithm.input.set(covariance::data, dataTable);
  algorithm.compute();
  services::SharedPtr<covariance::PartialResult> covariancePartResult;  
  covariancePartResult = algorithm.getPartialResult();
  
  if(rank == 0) {
    printf("Boss worker done.\n"); fflush(0);
    // Adding Boss worker's portion
    covariance::Distributed<step2Master> algorithmMaster;
    // algorithmMaster.parameter.outputMatrixType = covariance::correlationMatrix; // Use this for Correlation
    algorithmMaster.input.add(covariance::partialResults, covariancePartResult); 
    
    // Gathering all the results together
    MPI_Status stat;
    for(int i = 1; i < worldSize; i++) {
      int buffer_size;
      MPI_Recv(&buffer_size, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &stat);
      int worker = stat.MPI_SOURCE;

      // This is unnecessary allocation in our particular situation because buffer_size is always the
      // same. However, if the buffer_size changes from one process to another, this is required.
      byte recv_buffer[buffer_size] __attribute__((aligned(4096)));
      //byte* recv_buffer=(byte*)_mm_malloc(buffer_size, 4096); // Use if the buffer is too large
      MPI_Recv(recv_buffer, buffer_size, MPI_BYTE, worker, 0, MPI_COMM_WORLD, &stat);
      
      // Deserializing the received partial result and then adding it.
      OutputDataArchive receivedArchive(recv_buffer, buffer_size); 
      services::SharedPtr<covariance::PartialResult> receivedPartResult = services::SharedPtr<covariance::PartialResult>(new covariance::PartialResult());
      receivedPartResult->deserialize(receivedArchive);
      algorithmMaster.input.add(covariance::partialResults, receivedPartResult);
      printf("Received and added result from rank %d.\n", worker); fflush(0);
    }
    
    // Master computation.
    algorithmMaster.compute();


    algorithmMaster.finalizeCompute();
    services::SharedPtr<covariance::Result> covarianceResult;  
    covarianceResult= algorithmMaster.getResult();
    printf("Master computation complete.\n"); fflush(0);
    
    // Extracting the result.
    BlockDescriptor<double> result_block;
    covarianceResult->get(covariance::covariance)->getBlockOfRows(0, nVariables, readOnly, result_block);
    covarianceResult->get(covariance::correlation)->getBlockOfRows(0, nVariables, readOnly, result_block); // Use this for Correlation
    double *result = result_block.getBlockPtr();
    
    // Printing the part of the result.
    const int outputN = (10 < nVariables) ? 10 : nVariables;  
    printf("Printing the top left %dx%d corner\n", outputN, outputN);
    for(int i = 0; i < outputN; i++) {
      for(int j = 0; j < outputN; j++) {
	printf("%f ", result[i*nVariables+j]);
      } 
      printf("\n");
    } 
      
  } else {
    // Serialization of partial results
    InputDataArchive partResultArchive;
    covariancePartResult->serialize(partResultArchive);
    size_t buffer_size = partResultArchive.getSizeOfArchive();
    byte send_buffer[buffer_size] __attribute__((aligned(4096)));
    // byte* send_buffer=(byte*)_mm_malloc(buffer_size, 4096); // Use if the buffer is too large
    partResultArchive.copyArchiveToArray(send_buffer, buffer_size);
    
    // Sending the partial result
    MPI_Send(&buffer_size, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
    MPI_Send(send_buffer, buffer_size, MPI_BYTE, 0, 0, MPI_COMM_WORLD);
  }
  
  MPI_Finalize();
  return 0;
}
